import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  faGlobe, faEraser, faTrash, faPen, faPaintBrush, faPaintRoller,
  faGreaterThan, faLessThan, faSave, faDownload, faSpinner
} from '@fortawesome/free-solid-svg-icons';
import * as jspdf from 'jspdf';
import { saveAs } from 'file-saver';
import * as JSZip from 'jszip';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router, RoutesRecognized } from '@angular/router';

class Coords {
  x: number;
  y: number;

  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('shapeAnimateGr1', [
      state('open', style({ display: 'none' })),
      state('closed', style({ top: '0' })),
      transition('closed => open', [
        animate(2500, style({ top: '1100px' })),
        animate(200, style({ opacity: '0%' }))])
    ]),
    trigger('shapeAnimateGr2', [
      state('open', style({ display: 'none' })),
      state('closed', style({ top: '0' })),
      transition('closed => open', [
        animate(2300, style({ top: '1100px' })),
        animate(200, style({ opacity: '0%' }))])
    ]),
    trigger('shapeAnimateGr3', [
      state('open', style({ display: 'none' })),
      state('closed', style({ top: '0' })),
      transition('closed => open', [
        animate(2100, style({ top: '1100px' })),
        animate(200, style({ opacity: '0%' }))])
    ]),
    trigger('shapeAnimateGr4', [
      state('open', style({ display: 'none' })),
      state('closed', style({ top: '0' })),
      transition('closed => open', [
        animate(1500, style({ top: '1100px' })),
        animate(200, style({ opacity: '0%' }))])
    ]),
    trigger('shapeAnimateGr5', [
      state('open', style({ display: 'none' })),
      state('closed', style({ top: '0' })),
      transition('closed => open', [
        animate(1200, style({ top: '1100px' })),
        animate(200, style({ opacity: '0%' }))])
    ]),
    trigger('shapeAnimateGr6', [
      state('open', style({ display: 'none' })),
      state('closed', style({ top: '0' })),
      transition('closed => open', [
        animate(1000, style({ top: '1100px' })),
        animate(200, style({ opacity: '0%' }))])
    ]),
  ]
})
export class AppComponent implements OnInit {
  langEng: boolean = true;
  showLangBtn: boolean = true;
  page: number = 0;
  saving: string;

  faGlobe = faGlobe;
  faSave = faSave;
  faDownload = faDownload;
  faEraser = faEraser;
  faPen = faPen;
  faPaintBrush = faPaintBrush;
  faPaintRoller = faPaintRoller;
  faTrash = faTrash;
  faGreaterThan = faGreaterThan;
  faLessThan = faLessThan;

  lineColor = '#173E36';
  lineWidth = 1;
  mode: string = 'draw';
  tool: string = 'pen';
  activeCanvas: HTMLCanvasElement;
  lineCoords: Coords[] = [];

  showShapes1: boolean;
  showShapes2: boolean;
  showShapes3: boolean;
  showShapes4: boolean;
  showShapes5: boolean;
  showShapes6: boolean;

  @ViewChild('canvasCover', { static: true }) canvCoverElementRef: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasP1', { static: true }) canvP1ElementRef: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasP2', { static: true }) canvP2ElementRef: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasP3', { static: true }) canvP3ElementRef: ElementRef<HTMLCanvasElement>;
  @ViewChild('canvasP4', { static: true }) canvP4ElementRef: ElementRef<HTMLCanvasElement>;


  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((data) => {
      if (data instanceof RoutesRecognized) {
        const params = (data.state as any)._root.value.queryParams;
        this.showLangBtn = true;
        if (params['lang']) {
          if (params['lang'] === 'en') {
            this.langEng = true;
            this.showLangBtn = false;
          } else if (params['lang'] === 'ar') {
            this.langEng = false;
            this.showLangBtn = false;
          }
        }
      }
    });
    window.onresize = this.resizeCanvas;
    [this.canvasCover, this.canvasP1, this.canvasP2, this.canvasP3, this.canvasP4]
      .forEach(
        (canvas, i) => {
          canvas.width = canvas.parentElement.clientWidth;
          canvas.height = 1.43 * canvas.parentElement.clientWidth;
          canvas.addEventListener('mousedown', this.OnStart);
          canvas.addEventListener('mousemove', this.OnMove);
          canvas.addEventListener('mouseup', this.OnEnd);
          canvas.addEventListener('touchstart', (event: any) => {
            this.pickActiveCanvas();
          }, false);
          canvas.addEventListener('touchmove', this.OnTouchMove);
          canvas.addEventListener('touchend', this.OnEnd);
          canvas.addEventListener('touchcancel', this.OnEnd);
        }
      );
  }

  get canvasCover() {
    return this.canvCoverElementRef.nativeElement;
  }
  get canvasP1() {
    return this.canvP1ElementRef.nativeElement;
  }
  get canvasP2() {
    return this.canvP2ElementRef.nativeElement;
  }
  get canvasP3() {
    return this.canvP3ElementRef.nativeElement;
  }
  get canvasP4() {
    return this.canvP4ElementRef.nativeElement;
  }

  private resizeCanvas = () => {
    // console.log('bla');
    [this.canvasCover, this.canvasP1, this.canvasP2, this.canvasP3, this.canvasP4]
      .forEach(
        (canvas, i) => {
          canvas.width = canvas.parentElement.clientWidth;
          canvas.height = 1.43 * canvas.parentElement.clientWidth;
        }
      );
  }

  pickActiveCanvas() {
    if (this.page === 1) {
      this.activeCanvas = this.canvasP1;
    } else if (this.page === 2) {
      this.activeCanvas = this.canvasP2;
    } else if (this.page === 3) {
      this.activeCanvas = this.canvasP3;
    } else if (this.page === 4) {
      this.activeCanvas = this.canvasP4;
    }
  }

  switchToEnglish(langEng: boolean) {
    this.langEng = langEng;
  }

  nextPage() {
    if (this.page < 5) {
      this.page++;
      if (this.page === 5) {
        this.displayShapesWithDelay();
      }
    }
  }
  previousPage() {
    if (this.page > 0) {
      this.page--;
      this.hideShapes();
    }
  }

  clearCanvas() {
    this.pickActiveCanvas();
    this.tool = '';
    this.mode = 'clear';
    this.drawOnCanvas();
  }

  pickEraser() {
    this.mode = 'erase';
    this.tool = '';
  }

  pickColor(color: string) {
    this.mode = 'draw';
    this.lineColor = color;
    // picking the default tool in case
    // the eraser or the clear button was previously chosen
    if (!this.tool) {
      this.tool = 'pen';
      this.lineWidth = 1;
    }
  }

  pickTool(tool: string, lineWidth: number) {
    this.mode = 'draw';
    this.tool = tool;
    this.lineWidth = lineWidth;
  }

  private OnStart = (event: any) => {
    this.pickActiveCanvas();
  }

  private OnEnd = (event: any) => {
    this.activeCanvas = null;
    this.lineCoords = [];
  }

  private OnMove = (event: any) => {
    if (this.activeCanvas) {
      const x = event.offsetX || event.layerX - this.activeCanvas.offsetLeft;
      const y = event.offsetY || event.layerY - this.activeCanvas.offsetTop;

      this.lineCoords.push(new Coords(x, y));

      this.drawOnCanvas();
    }
  }

  private OnTouchMove = (event: any) => {
    const touch = event.touches[0];
    const mouseEvent = new MouseEvent('mousemove', {
      clientX: touch.clientX,
      clientY: touch.clientY
    });
    if (this.activeCanvas) {
      this.activeCanvas.dispatchEvent(mouseEvent);
      event.preventDefault();
      const x = mouseEvent.offsetX || mouseEvent.layerX - this.activeCanvas.offsetLeft;
      const y = mouseEvent.offsetY || mouseEvent.layerY - this.activeCanvas.offsetTop;

      this.lineCoords.push(new Coords(x, y));

      this.drawOnCanvas();
    }
  }

  drawOnCanvas() {
    const ctx = this.activeCanvas.getContext('2d');
    if (this.mode === 'clear') {
      ctx.clearRect(0, 0, this.activeCanvas.width, this.activeCanvas.height);
      // pick up the default tool
      this.pickTool('pen', 1);
      this.activeCanvas = null;
      this.lineCoords = [];
    } else {
      if (this.mode === 'erase') {
        ctx.globalCompositeOperation = 'destination-out';
        ctx.lineWidth = 40;
      } else {
        ctx.globalCompositeOperation = 'source-over';
        ctx.strokeStyle = this.lineColor;
        ctx.lineWidth = this.lineWidth;
      }

      ctx.beginPath();
      ctx.moveTo(this.lineCoords[0].x, this.lineCoords[0].y);

      for (const coord of this.lineCoords) {
        ctx.lineTo(coord.x, coord.y);
      }
      ctx.stroke();
    }
  }


  getBgImagePath(pageNo: number) {
    let lang = '';
    if (this.langEng) {
      lang = '../assets/en_workbook_jpegs/';
    } else {
      lang = '../assets/ar_workbook_jpegs/';
    }
    if (pageNo === 0) {
      return lang + 'cover.jpg';
    } else {
      return lang + 'p' + pageNo + '.jpg';
    }
  }

  savePageAsJPEG(withDrawings: boolean) {
    this.saving = 'page';
    const pathToImage = this.getBgImagePath(this.page);
    const splitPath = pathToImage.split('/');
    const filename = 'ucl-exhibition-workbook-' + pathToImage.split('/')[splitPath.length - 1];
    if (withDrawings) {
      this.pickActiveCanvas();
      const img = new Image();
      img.src = pathToImage;
      // create new canvas
      const newCanvas = document.createElement('canvas');
      newCanvas.width = this.activeCanvas.width;
      newCanvas.height = this.activeCanvas.height;
      const ctx = newCanvas.getContext('2d');
      img.onload = () => {
        console.log('image loaded');
        // draw background image
        ctx.drawImage(img, 0, 0, newCanvas.width, newCanvas.height);
        // draw page context
        ctx.drawImage(this.activeCanvas, 0, 0);
        const canvasToURL = newCanvas.toDataURL('image/jpeg', 1);
        newCanvas.remove();
        setTimeout(() => {
          saveAs(canvasToURL, filename);
          this.saving = '';
        }, 1000);
        this.activeCanvas = null;
        this.lineCoords = [];
      };
    } else {
      setTimeout(() => {
        saveAs(pathToImage, filename);
        this.saving = '';
      }, 1000);
    }
}

  saveWorkbookAsJPEG(withDrawings: boolean) {
    this.saving = withDrawings ? 'jpegWith' : 'jpegWithout';
    const filename = 'ucl-exhibition-workbook.zip';
    const files = [];

    const promises = [];
    promises.push(Promise.resolve(1));

    [this.canvasCover, this.canvasP1, this.canvasP2, this.canvasP3, this.canvasP4]
      .forEach(
        (canvas, i) => {
          const img = new Image();
          img.src = this.getBgImagePath(i);
          const newCanvas = document.createElement('canvas');
          newCanvas.width = canvas.width;
          newCanvas.height = canvas.height;
          const ctx = newCanvas.getContext('2d');
          promises.push ( new Promise( (resolve, reject) => {
            img.onload = () => {
              ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
              if (withDrawings) {
                ctx.drawImage(canvas, 0, 0);
              }
              const canvasToURL = newCanvas.toDataURL('image/jpeg', 1);
              files.push(canvasToURL.replace(/^data:image\/jpeg;base64,/, ''));
              newCanvas.remove();
              resolve();
            };
        }));
        }
      );

      Promise.all(promises).then( () => {
        const zip = new JSZip();
        for (let i = 0; i < files.length; i++) {
          if (i === 0) {
            zip.file('workbook-cover.jpg', files[i], { base64: true });
          } else {
            zip.file('workbook-p' + i + '.jpg', files[i], { base64: true });
          }
        }
        setTimeout( () => {
          zip.generateAsync({ type: 'blob' }).then((content) => {
            if (content) {
              saveAs(content, filename);
              this.saving = '';
            }
          });
        }, 1000);
      }
      );
  }

  saveWorkbookAsPDF(withDrawings: boolean) {
    this.saving = withDrawings ? 'pdfWith' : 'pdfWithout';
    const pdf = new jspdf();
    const imgWidth = pdf.internal.pageSize.width - 2;
    const imgHeight = this.canvasP1.height * imgWidth / this.canvasP1.width;

    let pathToImage = this.getBgImagePath(0);
    let img = document.createElement('img');
    img.setAttribute('src', pathToImage);
    pdf.addImage(img, 'PNG', 1, 0, imgWidth, imgHeight, '', 'FAST');
    img.remove();
    [this.canvasP1, this.canvasP2, this.canvasP3, this.canvasP4]
      .forEach(
        (canvas, i) => {
          pdf.addPage();
          pathToImage = this.getBgImagePath(i + 1);
          img = document.createElement('img');
          img.setAttribute('src', pathToImage);
          pdf.addImage(img, 'PNG', 1, 0, imgWidth, imgHeight, '', 'FAST');
          img.remove();
          if (withDrawings) {
            const canvasToURL = canvas.toDataURL('image/png', 1);
            pdf.addImage(canvasToURL, 'PNG', 1, 0, imgWidth, imgHeight, '', 'FAST');
          }
        }
      );

    setTimeout( () => {
      const filename = 'ucl-exhibition-workbook.pdf';
      const file = new Blob([pdf.output('blob', { title: filename })], { type: 'application/pdf' });
      saveAs(file, filename);
      this.saving = '';
    }, 1000);
  }


  displayShapesWithDelay() {
    this.showShapes1 = true;
    setTimeout(() => this.showShapes2 = true, 200);
    setTimeout(() => this.showShapes3 = true, 400);
    setTimeout(() => this.showShapes4 = true, 600);
    setTimeout(() => this.showShapes5 = true, 800);
    setTimeout(() => this.showShapes6 = true, 1000);
  }


  hideShapes() {
    this.showShapes1 = false;
    this.showShapes2 = false;
    this.showShapes3 = false;
    this.showShapes4 = false;
    this.showShapes5 = false;
    this.showShapes6 = false;
  }

}
